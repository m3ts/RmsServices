var express = require('express'),
    bodyParser = require('body-parser'),
    errorHandler = require('errorhandler'),
    logger = require('morgan'),
    expressSession = require('express-session')
    fs = require('fs');

var app = express();
var port = 0;

var settings = JSON.parse(fs.readFileSync(__dirname + '/settings.json').toString());
port = settings.port;

// all environments
app.set('port', process.env.PORT || port);
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
//app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSession({resave:true, saveUninitialized:true, secret:'M30K!kmsp01', cookie:{maxAge:1000 * 60 * 60 * 24}}));


// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

app.auth = function(req, res, next)
{
  console.log(req.session.operatorId + ' ' + req.session.userName);
  if(req.session.operatorId != undefined && req.session.userName != undefined)
    next();
  else
    res.status(401).send('Not Logged In');
};

//Dynamically Add Routes from /Routes
console.log('Loading routes from: ' + settings.routePath);
fs.readdirSync(settings.routePath).forEach(function(file) {
    var route = settings.routePath + file.substr(0, file.indexOf('.'));
    console.log('Adding route:' + route);
    require('./' + route)(app);
});

app.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});