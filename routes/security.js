var db = require('../db/Security/RmsSecurityDb');

module.exports = function(app) {  
  var baseUrl = '/m3t/Rms/Security/';
  
  //don't use auth on this, so we can log invalid logins
  app.get(baseUrl + 'BadLogon/:user/:wkstn/:apptype/:logintype/:success/:opId', BadLogon);
  
  app.get(baseUrl + 'Login/:operatorId/:userName/:pwhash', Login);
  app.post(baseUrl + 'UserUpdate', app.auth, UserUpdate);
  app.get(baseUrl + 'UserLogon/:id/:operatorId', UserLogon);
  app.get(baseUrl + 'GetConfig/:configId/:operatorId', GetConfig);
  app.get(baseUrl + 'IsLoggedIn', app.auth, CheckLogin);
  
  app.get(baseUrl + 'CheckActive/:op', app.auth, CheckActive);
  app.get(baseUrl + 'WorkStationLogon/:userId/:workStationId/:ipAddress/:appType/:appVersion/:operatorId', WorkStationLogon);
}


function WorkStationLogon(req, res) {
  db.WorkStationLogon( req.params.userId, req.params.workStationId, req.params.ipAddress, req.params.appType,
                       req.params.appVersion, req.params.operatorId, function(err,data) {
    if(err) {
     res.status(400).send('Database Error' + err + ' ' + data);
    } else {
       res.status(200).send(data);
    }
  });
}
function CheckActive(req, res) {
 db.CheckOperatorActive(req.params.op, function(err,data) {
   if(err) {
      res.status(400).send('Database Error');
   } else {
      res.status(200).send(data);
   }
 });
}
function CheckLogin(req, res) {
  res.status(200).send('You\'re logged in');
}

function UserUpdate(req, res) {

  console.log('sess ' + req.session.operatorId + ' ' + req.session.userName);
  db.UpdateUser(req.body.operatorId, req.body.UserId, req.body.PwdHash, req.body.PwdSalt, req.body.PasswordExpires,
                  req.body.UpdatedBy, req.body.UpdatedFrom, function ( err, data) {
        if (err) {
          res.status(201).send('');
        } else {
          res.status(200).send(data);
        }
      });
}


function BadLogon(req, res) {
 db.BadLogon( req.params.user, req.params.wkstn, req.params.apptype, req.params.logintype, req.params.success, req.params.opId, function(err,data) {
   if(err) {
      res.status(400).send('Database Error');
   } else {
      res.status(200).send('Logged');
   }
 });
}

function Login(req, res) {
  //I'm not sure what we should do to auth the user
  //maybe pass up the password with https, then re-hash it and compare to the database?
  //For now it's not checking the pwhash variable
  console.log(req.params.operatorId + ' ' + req.params.userName + ' ' + req.params.pwhash);
  
  db.CheckLogin(req.params.operatorId, req.params.userName, req.params.pwhash, function ( err, data) {
        if (err) {
          res.status(401).send('Invalid Credentials');
        } else {
          //set session vars
          req.session.operatorId = req.params.operatorId;
          req.session.userName = req.params.userName;
          
          console.log('added sess ' + req.session.operatorId + ' ' + req.session.userName);
          res.status(200).send('Logged In');
        }
      });
}

function UserLogon(req, res) {  
  db.UserLogon( req.params.id, req.params.operatorId, function ( err, data) {
        if (err) {
          res.status(201).send('');
        } else {
          res.status(200).send(data);
        }
      });
}

function GetConfig(req, res) {
  console.log(req.params.operatorId + ' ' + req.params.configId);
  
  db.GetConfig( req.params.configId, req.params.operatorId, function ( err, data) {
        if (err) {
          res.status(201).send('');
        } else {
          res.status(200).send(data);
        }
      });
}