
var ConnectionPool = require('tedious-connection-pool');
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var fs = require('fs');

function callback(error,results){}


var settings = JSON.parse(fs.readFileSync('./settings.json').toString());


var config = {
  userName: 'cms',
  password: 'cms',
  //server: 'rms.m3ts.com',
  server: settings.sqlServer,
  tdsVersion: '7_2',
  options: {
    encrypt: false,
    //database: 'm3_Fed_Root',
    database: settings.db,
    instanceName: settings.instance,
    connectTimeout: 10000,
    requestTimeout: 10000,
    //port: 38192,
        //     debug: {
        //        packet:  true,
        //        data:    true,
        //        payload: true,
        //        log:     true 
        // }
    }
 
};



var poolConfig = {
   min: 0,
   max: 40,
   idleTimeoutMillis: 3000
};

var pool = new ConnectionPool(poolConfig, config);


function GetDbConnection(cb) {
  pool.requestConnection(function (err, connection) {
    if (err) {
      console.log(err);
      return cb(err, null);
    }
      //console.log('connected from pool');
    connection.on('connect', function (err) {
      if (err) {
        console.log(err);
         return cb(err, null);
      } 
        //console.log('***Connected***');
        return cb(null, connection);
      
    });

    connection.on('end', function (err) {
         //console.log('Connection closed') ;
    });
  });
}exports.GetDbConnection = GetDbConnection;