var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var dbConnect = require('../DbConnectionPool');

function CheckLogin(operatorId, userName, pwhash, cb)
{
  dbConnect.GetDbConnection(function ( err, connection ) {
  
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'SELECT * FROM m3_fed_root..sc_users WHERE operatorId = @opId AND user_id = @userName ';

      var request = new Request(sql, function (err, result) {
          console.log('Login attempt');
        if (err) {
          console.log('Login failed');
          connection.close();
          return  cb('Database error', null);
        } else {
          console.log('Login ok');
          connection.close();
          cb( null, 'OK');
        }
      }); 
      
      request.addParameter('userName', TYPES.VarChar, userName);
      request.addParameter('opId', TYPES.Int, operatorId);
      request.addParameter('pwdHash', TYPES.VarChar, pwhash);

      connection.execSql(request);  
    }

  });
}

function UpdateUser(operatorId, userId, pwdHash, pwdSalt, passwordExpires, updatedBy, updatedFrom, cb ) {
  dbConnect.GetDbConnection(function ( err, connection ) {
  
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'update m3_fed_root..sc_users set pwdHash = @pwdHash, pwdSalt = @pwdSalt, password_expires = @pwdExpires, updated_by = @updatedBy, ' +
                'updated = getdate(), updated_from = @updatedFrom, pwdLastChanged = getdate(), pwdLastChangedBy = @updatedBy where ' +
                'user_id = @userId and operatorId = @oper';

      var request = new Request(sql, function (err, result) {
      
          console.log('Update attempt');
        if (err) {
          console.log('Update failed');
          connection.close();
          return  cb('Database error', null);
        } else {
          console.log('Update ok');
          connection.close();
          cb( null, 'OK');
        }
      }); 
      
      request.addParameter('userId', TYPES.VarChar, userId);
      request.addParameter('oper', TYPES.Int, operatorId);
      request.addParameter('pwdHash', TYPES.VarChar, pwdHash);
      request.addParameter('pwdSalt', TYPES.VarChar, pwdSalt);
      
      request.addParameter('pwdExpires', TYPES.DateTime, new Date(passwordExpires));
      request.addParameter('updatedby', TYPES.VarChar, updatedBy);
      request.addParameter('updatedfrom', TYPES.VarChar, updatedFrom);

      connection.execSql(request);  
    }

  });

  
}

function BadLogon( userId, workStationId, appType, logonType, success, operatorId, cb ) {
  console.log('In Badlogon');
  console.log('Success = ' + success);
  var status = -1;

  if (success === "False") {
    status = 0;
  } else {
    status = 1;
  }
  dbConnect.GetDbConnection(function ( err, connection ) {
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'insert into m3_fed_root..db_badlogons( operatorId, login_type, app_type, updated_by, updated_from, updated, ' +
                'success) values( @oper, @type, @apptype, @user, @workstation, getdate(), @success)';

      var request =  new Request(sql, function (err, result) {
        if (err) {
          console.log('BadLogon error: ' + err);
          connection.close();
          return cb('Database error', null);
        } else {
          connection.close();
          cb(null, 'OK');
        }
      });
      request.addParameter('oper', TYPES.Int, operatorId);
      request.addParameter('type', TYPES.Int, logonType);
      request.addParameter('apptype', TYPES.Int, appType);
      request.addParameter('user', TYPES.VarChar, userId);
      request.addParameter('workstation', TYPES.VarChar, workStationId);
      request.addParameter('success', TYPES.Bit, status);

      connection.execSql(request);  

    }
  });

}

function WorkStationCheckStatus( workStationId, appType, operatorId, cb ) {
  var status = -1;
  
  dbConnect.GetDbConnection(function ( err, connection ) {
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'select status from m3_fed_root..sc_workstations where workstation_id = @id and appType = @type and operatorId = @oper';

      var request = new Request(sql, function (err, result) {
        if (err) {
          connection.close();
          return cb('Database error', null);
        } else {
          connection.close();
          return cb(null, status);
        }
      });

      request.on('row', function (columns) {
        status = columns[0].value;
      });

      request.addParameter('id', TYPES.VarChar, workstationId);
      request.addParameter('type', TYPES.Int, appType);
      request.addParameter('oper', TYPES.Int, operatorId);

      connection.execSql(request);
    }
  });
}


function WorkStationLogon( userId, workStationId, ipAddress, appType, appVersion, operatorId, cb ) {
  var status = true;

  dbConnect.GetDbConnection(function (err, connection) {
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'update m3_fed_root..sc_workstations set logonIpAddress = @ip, logonUser = @user, lastLogon = getdate(), ' +
                'lastLogOff = null, appVersion = @version where workStation_Id = @id and appType = @type AND operatorID = @operatorId';

      var request = new Request(sql, function ( err, result ) {
        if (err) {
          connection.close();
          return cb('Database error' + err + ' ' + result, null);
        } else {
          connection.close();
          return cb(null, status);
        }
      }); 
      
      request.addParameter('user', TYPES.VarChar, userId);
      request.addParameter('id', TYPES.VarChar, workStationId);
      request.addParameter('ip', TYPES.VarChar, ipAddress);
      request.addParameter('type', TYPES.VarChar, appType);
      request.addParameter('version', TYPES.VarChar, appVersion);
      request.addParameter('operatorId', TYPES.VarChar, operatorId);

      connection.execSql(request);

    }
  });
}

function UserLogon( userId, operatorId, cb ) {
  Logon( userId, operatorId, function (err, users) {
    if (err) {
      console.log('1');
      return cb(err, users);
    } else {
      GetOptions(users.SecurityGroupId, 7, function (err, options) {
        if (err) {
          console.log('2');
          return cb(err, users);
        } else {
          users.Options = options;
          GetForms(users.SecurityGroupId, 7, operatorId, function (err, forms) {
            if (err) {
              console.log('3');
              return cb(err, users);
            } else {
              users.Forms = forms;
              GetMenus(users.SecurityGroupId, 7, operatorId, function (err, menus) {
                if (err) {
                  console.log('4');
                  return cb(err, users);
                } else {
                  users.Menus = menus;
                  cb(null, users);
                }
              });
            }
          });

        }
      });
    }
  });
}


function Logon( userId, operatorId, cb ) {

  dbConnect.GetDbConnection(function (err, connection) {
    var userInfo = [];
    var userInfo1 = {};
    if (err) {
      return cb('Database error', null); 
    } else {
      var sql = 'select groupId, status, password_expires, start_form, pwdHash, pwdSalt, operatorId from m3_fed_root..sc_users where ' +
                'user_Id = @id and operatorId = @oper';

      var request = new Request(sql, function (err, result) {
        if (err) {
          console.log('Logon error: ' + err);
          connection.close();
          return cb('Database error', userInfo);
        } else {
          console.log('Logon ok');
          connection.close();
          cb(null, userInfo1);
        }
      }); 

      request.on('row', function (columns) {
        userInfo1.UserInfo = userId;
        userInfo1.Status =  columns[1].value;
        userInfo1.StartForm = columns[3].value;
        userInfo1.SecurityGroupId = columns[0].value;
        userInfo1.PasswordExpires = columns[2].value;
        userInfo1.PasswordSalt = columns[5].value;
        userInfo1.PasswordHash = columns[4].value;
        userInfo1.Operation = operatorId;
        //userInfo1.FormRestrict = [0,0];
        //userInfo1.OptionRestrict = [0,0];


        // userInfo.push({
        //   Username        : userId,
        //   Status          : columns[1].value,
        //   StartForm       : columns[3].value,
        //   SecurityGroupId : columns[0].value,
        //   PasswordExpires : columns[2].value,
        //   PasswordSalt    : columns[5].value,
        //   PasswordHash    : columns[4].value,
        //   Operator        : operatorId,
        //   FormRestrict    : [],
        //   OptionRestrict  : []
        // });
      });    

      request.addParameter('id', TYPES.VarChar, userId);
      request.addParameter('oper', TYPES.Int, operatorId);

      connection.execSql(request);        

    }
  });
}

function GetOptions( userGroup, appType, cb ){
  var options = [];
  var options1 = {};

  dbConnect.GetDbConnection(function (err, connection) {
    if (err){
      return cb('Database error', null);
    } else {
      var sql = 'select sc_assigned_options.form_id, sc_assigned_options.option_id, cf_options.option_display, cf_options.workstation_restrict, ' +
                'cf_options.security_level from m3_fed_root..sc_assigned_options, m3_fed_root..cf_options where sc_assigned_options.group_id = ' +
                '@groupid and sc_assigned_options.form_id = cf_options.form_id and sc_assigned_options.option_id = cf_options.option_id and ' +
                'cf_options.app_Type = @apptype order by sc_assigned_options.form_id, sc_assigned_options.option_id';

      var request = new Request(sql, function (err, result) {
        if (err) {
          console.log('GetOptions error: ' + err);
          connection.close();
          return cb('Database error', options);
        } else {
          connection.close();
          return cb(null, options);
        }
      }); 
      
      request.on('row', function (columns) {
        // options1.FormID = columns[0].value;
        // options1.ID = columns[1].value;
        // options1.OptionDisplay = columns[2].value;
        // options1.SecurityLevel = columns[4].value;
        // options1.WorkstationRestrict = columns[3].value;

        options.push({
          FormID              : columns[0].value,
          ID                  : columns[1].value,
          OptionDisplay       : columns[2].value,
          SecurityLevel       : columns[4].value,
          WorkstationRestrict : columns[3].value


         });
      });
       request.addParameter('groupid', TYPES.VarChar, userGroup);
       request.addParameter('apptype', TYPES.Int, appType);

       connection.execSql(request);    
    }
  });
}

function GetForms( userGroup, appType, operatorId, cb ) {
  var forms = [];
  var forms1 = {};

  dbConnect.GetDbConnection(function (err, connection) {
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'select sc_assigned_forms.form_id, sc_assigned_forms.menu_id, sc_assigned_forms.form_seq, cf_forms.form_desc, ' +
                'cf_forms.assembly_id, cf_forms.workstation_restrict, cf_forms.form_class, cf_forms.security_level from m3_fed_root..sc_assigned_forms ' +
                'join m3_fed_root..cf_forms on sc_assigned_forms.form_id = cf_forms.form_id where sc_assigned_forms.group_id = @id and '  +
                'cf_forms.app_type = @apptype and sc_assigned_forms.operatorid = @oper order by sc_assigned_forms.menu_id, sc_assigned_forms.form_seq';

      var request = new Request(sql, function (err, result ) {
        if (err) {
          console.log('GetForms error: ' + err);
          connection.close();
          return cb('Database error', null);
        } else {
          connection.close();
          return cb(null, forms);
        }
      });  

      request.on('row', function (columns) {
        // forms1.ID = columns[0].value;
        // forms1.Sequence = columns[2].value;
        // forms1.Description = columns[3].value;
        // forms1.AssemblyId = columns[4].value;
        // forms1.FormClass = columns[6].value;
        // forms1.MenuID = columns[1].value;
        // forms1.SecurityLevel = columns[7].value;
        // forms1.WorkstationRestrict = columns[5].value;


        forms.push({
          ID                  : columns[0].value,
          Sequence            : columns[2].value,
          Description         : columns[3].value.trim(),
          AssemblyId          : columns[4].value,
          FormClass           : columns[6].value.trim(),
          MenuID              : columns[1].value,
          SecurityLevel       : columns[7].value,
          WorkstationRestrict : columns[5].value
        });
      }); 

      request.addParameter('id', TYPES.VarChar, userGroup);
      request.addParameter('apptype', TYPES.Int, appType);
      request.addParameter('oper', TYPES.Int, operatorId);

      connection.execSql(request);  
    }
  });
}

function GetMenus( userGroup, appType, operatorId, cb ) {
  var menus = [];
  var menus1 = {};

  dbConnect.GetDbConnection(function (err, connection) {
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'select sc_assigned_menus.menu_seq, sc_assigned_menus.menu_id, sc_menus.menu_desc from m3_fed_root..sc_assigned_menus ' +
                'join m3_fed_root..sc_menus on sc_assigned_menus.menu_id = sc_menus.menu_id where sc_assigned_menus.group_id = @id ' +
                'and sc_menus.app_type = @type and sc_assigned_menus.operatorId = @oper order by sc_assigned_menus.menu_seq';

      var request = new Request(sql, function (err, result) {
        if (err) {
          console.log('GetMenus error: ' + err);
          connection.close();
          return cb('Database error', null);
        } else {
          connection.close();
          return cb(null, menus);
        }
      });

      request.on('row', function (columns) {
        // menus1.ID = columns[1].value;
        // menus1.Sequence = columns[0].value;
        // menus1.Description = columns[2].value.trim();

        menus.push({
          ID          : columns[1].value,
          Sequence    : columns[0].value,
          Description : columns[2].value.trim()

         });
      }); 
      request.addParameter('id', TYPES.VarChar, userGroup);
      request.addParameter('type', TYPES.Int, appType);
      request.addParameter('oper', TYPES.Int, operatorId);

      connection.execSql(request);  

    }
  });
}

function OperatorCodeCheck( operatorId, code, cb ) {
  var valid = 0;

  dbConnect.GetDbConnection(function (err, connection) {
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'select active from m3_fed_root..cf_operators where id = @id and securitycode = @code';

      var request = new Request(sql, function (err, result) {
        if (err) {
          connection.close();
          return cb('Database error', null);
        } else {
          connection.close();
          return cb(null, valid);
        }
      });

      request.on('row', function (columns) {
        valid = 1;
      });
      request.addParameter('id', TYPES.Int, operatorId);
      request.addParameter('code', TYPES.VarChar, code);

      connection.execSql(request);  

    }
  });
}


function CheckOperatorActive( operatorId, cb ) {
  var active = 0;

  dbConnect.GetDbConnection(function (err, connection) {
    if (err) {
      return cb('Database error', null);
    } else {
      var sql = 'select active from cf_operators where id = @id';

        var request = new Request(sql, function (err, result ) {
          if (err) {
            connection.close();
            return cb('Database error', null);
          } else {
            connection.close();
            return cb(null, active);
          }
        });

        request.on('row', function (columns) {
          active = columns[0].value;
        });

        request.addParameter('id', TYPES.Int, operatorId);

        connection.execSql(request);  

    }
  });
}


function LogError( module, form, desc, value, userId, workstation, operatorId, cb ) {
  dbConnect.GetDbConnection(function (err, connection) {
  if (err) {
    return cb('Database error', null);   
  } else {
    var sql = 'insert into m3_fed_root..db_errors(operatorId, module_id, form_id, function_desc, error_desc, updated_by, ' +
              'updated_from, updated) values( @oper, @module, @form, @desc, @edesc, @updatedby, @updatedfrom, getdate())';
    
    var request = new Request(sql, function ( err, result ) {
      if (err) {
        console.log('LogError error = ' + err);
        connection.close();
        return cb('Database error', null);
      } else {
        connection.close();
        return cb(null,'OK');
      }
    });

    request.addParameter('oper', TYPES.Int, operatorId);
    request.addParameter('module', TYPES.Int, module);
    request.addParameter('form', TYPES.Int, form);
    request.addParameter('desc', TYPES.VarChar, desc);
    request.addParameter('edesc', TYPES.VarChar, value);
    request.addParameter('updatedby', TYPES.VarChar, userId);
    request.addParameter('updatedfrom', TYPES.VarChar, workstation);

    connection.execSql(request);  

  }
  });
}

function GetConfig( configId, operatorId, cb ) {

  dbConnect.GetDbConnection(function (err, connection) {
    if (err) {
      return cb('Database error', null);
    } else {
      var configValue = '';
      var sql = 'select config_value from m3_fed_root..sc_configure where config_id = @id and operatorId = @oper';

      var request = new Request(sql, function ( err, result ) {
        if (err) {
          connection.close();
          console.log('GetConfig error = ' + err);
          return cb('Database error', null);
        } else {
          connection.close();
          return cb(null, configValue);
        }
      });

      request.on('row', function (columns) {
        columns.forEach(function(el,ndx,arr) {console.log(el.metadata.colName + ': ' + el.value.trim());});
        configValue = columns[0].value.trim();
      });
      
      request.addParameter('id', TYPES.VarChar, configId);
      request.addParameter('oper', TYPES.Int, operatorId);

      connection.execSql(request);  
    }
  });
}


module.exports = {
  UpdateUser             : UpdateUser,
  BadLogon               : BadLogon,
  WorkStationCheckStatus : WorkStationCheckStatus,
  WorkStationLogon       : WorkStationLogon,
  UserLogon              : UserLogon,
  OperatorCodeCheck      : OperatorCodeCheck,
  CheckOperatorActive    : CheckOperatorActive,
  LogError               : LogError,
  GetConfig              : GetConfig,
  CheckLogin             : CheckLogin  
};